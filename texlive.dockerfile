FROM debian:bullseye
RUN apt-get update && apt-get install -y texlive-full
RUN adduser tex --home /home/tex
USER tex
VOLUME /home/tex/workdir
WORKDIR /home/tex/workdir
