BrentSchroeter.pdf: main.tex structure.tex fonts
	docker build -f texlive.dockerfile -t texlive:local .
	docker run --rm -itv "$$PWD:/home/tex/workdir" texlive:local xelatex -jobname BrentSchroeter main.tex
	rm *.out *.aux *.log
